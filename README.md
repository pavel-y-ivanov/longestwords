# Task
 - In the programming language of your choice create a class with a method to return the length and longest words in a sentence. For example, “The cow jumped over the moon.” should return 6 and “jumped”.
 - Create unit tests to test that method, reworking your code if needed.
 - Explain any assumptions in comments.
 - Add a README explaining how to execute your tests.

# Environment information
 - This project was created using [IntelliJ IDEA](https://www.jetbrains.com/idea/download/) (can be opened in it without additional setup).
 - It uses [maven](https://maven.apache.org/) - make sure it's installed in your IDE (or system-wide to run from terminal/command line).
 - Source files follow [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html) (IDE-specific xml-config files can be downloaded from [here](https://github.com/google/styleguide))

# Running tests
Precondition: Checked out source from this repository.
## Command line
 1. Switch to `longestwords` directory.
 2. Run `mvn test` (or `mvn site` to get HTML report).
 3. Test results will be displayed in the command line (as well as in HTML report under maven's `target/site` folder if this option was used).

## IntelliJ IDEA
 1. Open `longestwords` project.
 2. Right-click on the project.
 3. Click `Run 'All tests'`.
 4. IDE will display test results according to your configuration.
