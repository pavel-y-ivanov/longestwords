package tk.pavelivanov.solutions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class LongestWordsParametrizedTests {

  @Parameterized.Parameters(name = "(\"{0}\") = Result(words={1},length={2})")
  public static Iterable<Object[]> data() {
    return Arrays.asList(new Object[][]{
        {"The cow jumped over the moon.", new HashSet<String>(Arrays.asList("jumped")), 6},
        {"each word same", new HashSet<String>(Arrays.asList("each", "word", "same")), 4},
        {"one[two),-3, mo!", new HashSet<String>(Arrays.asList("one", "two")), 3},
        {"onelongwordnobreaks", new HashSet<String>(Arrays.asList("onelongwordnobreaks")), 19},
        {"a", new HashSet<String>(Arrays.asList("a")), 1},
        {"xx, yy|zz", new HashSet<String>(Arrays.asList("xx", "yy", "zz")), 2},
        {"", new HashSet<String>(), 0},
        {"\t ", new HashSet<String>(), 0},
        {"!@#$%^&*()", new HashSet<String>(), 0}
    });
  }

  private String inputSentence;
  private Set<String> expectedResultWords;
  private int expectedResultLength;

  private LongestWords longestWords;

  public LongestWordsParametrizedTests(String inputSentence, HashSet<String> expectedResultWords,
      int expectedResultLength) {
    this.inputSentence = inputSentence;
    this.expectedResultWords = expectedResultWords;
    this.expectedResultLength = expectedResultLength;
  }

  @Before
  public void setUp() throws Exception {
    longestWords = new LongestWords();
  }

  @After
  public void tearDown() throws Exception {
    longestWords = null;
  }

  @Test
  public void testFindLongestWords() {
    LongestWords.Result result = longestWords.findLongestWords(inputSentence);
    assertEquals("Result words must match expected", expectedResultWords, result.getWords());
    assertEquals("Result length must match expected", expectedResultLength, result.getLength());
  }

  @Test
  public void testFindLongestWordsOptimized() {
    LongestWords.Result result = longestWords.findLongestWordsOptimized(inputSentence);
    assertEquals("Result words must match expected", expectedResultWords, result.getWords());
    assertEquals("Result length must match expected", expectedResultLength, result.getLength());
  }
}