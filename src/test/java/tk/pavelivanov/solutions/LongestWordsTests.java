package tk.pavelivanov.solutions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LongestWordsTests {

  private LongestWords longestWords;

  @Before
  public void setUp() throws Exception {
    longestWords = new LongestWords();
  }

  @After
  public void tearDown() throws Exception {
    longestWords = null;
  }

  @Test(expected = IllegalArgumentException.class)
  public void testFindLongestWordsThrowsIllegalArgumentExceptionForNullInput() {
    longestWords.findLongestWords(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testFindLongestWordsOptimizedThrowsIllegalArgumentExceptionForNullInput() {
    longestWords.findLongestWordsOptimized(null);
  }
}
