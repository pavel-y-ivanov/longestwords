package tk.pavelivanov.solutions;

import java.util.HashSet;
import java.util.Set;

public class LongestWords {

  /**
   * Result holds a set of words and their length. Assuming that only unique words are needed. If
   * repeating words should be included, set can be replaced with array or list.
   */
  public class Result {

    public Set<String> getWords() {
      return words;
    }

    public int getLength() {
      return length;
    }

    private Set<String> words;
    private int length;

    public Result(Set<String> words) {
      this.words = words;
      int length = 0;
      if (words != null && words.size() > 0) {
        length = words.iterator().next().length();
      }
      this.length = length;
    }
  }

  /**
   * Returns a new LongestWords.Result with the longest words from a sentence and the word's length.
   * Only continuous letters are accounted for. Regex in the code will need to be changed to use
   * this on more complex cases like hyphenated words (e.g. self-made), words with apostrophes (e.g.
   * can't), abbreviations with periods (e.g. U.A.E.), etc.
   *
   * Time complexity: O(n+m) where: n - total number of characters in the input sentence, m - total
   * number of words in the input sentence.
   *
   * Explanation: O(n) to split the sentence + O(m) to iterate over the array and find max length +
   * O(m) to iterate over the array and collect the longest words = O(n+2m) ≈ O(n+m).
   *
   * Space complexity: O(m+k) where: m - total number of words in the input sentence, k - number of
   * unique words of max length in the input sentence.
   *
   * Explanation: O(m) to hold the array of words after split + O(k) to hold the result set =
   * O(m+k).
   *
   * @param sentence the sentence to search for longest words in; string with no words (whitespaces,
   * punctuation, etc.) is considered valid and returns a Result holding an empty set and 0 as
   * length.
   * @return a new LongestWords.Result holding the longest words from the sentence and the length.
   * @throws IllegalArgumentException if input sentence is null.
   */
  public Result findLongestWords(String sentence) throws IllegalArgumentException {
    if (sentence == null) {
      throw new IllegalArgumentException("Input sentence cannot be null");
    }

    String trimmedSentence = sentence.trim();
    if (trimmedSentence.isEmpty()) {
      return new Result(new HashSet<String>());
    }
    String[] allWords = trimmedSentence.split("\\W+");

    // Find max length
    int maxLength = 0;
    for (String word : allWords) {
      int currentWordLenght = word.length();
      if (currentWordLenght > maxLength) {
        maxLength = currentWordLenght;
      }
    }

    // Collect all words of maxLength
    Set<String> longestWords = new HashSet<String>();
    for (String word : allWords) {
      if (word.length() == maxLength) {
        longestWords.add(word);
      }
    }

    return new Result(longestWords);
  }

  /**
   * Less readable, but optimized version of findLongestWords. This one will always perform better
   * time-wise, and also will perform better memory-wise in most cases, except when the longest
   * word's length is close to the input's length (will end up holding almost all characters in
   * memory anyway in this case).
   *
   * Returns a new LongestWords.Result with the longest words from a sentence and the word's length.
   * Only continuous letters are accounted for. Character check condition in the code will need to
   * be changed to use this on more complex cases like hyphenated words (e.g. self-made), words with
   * apostrophes (e.g. can't), abbreviations with periods (e.g. U.A.E.), etc.
   *
   * Time complexity: O(n) where: n - total number of characters in the input sentence.
   *
   * Explanation: O(n) to iterate over all the characters and find maxLength.
   *
   * Space complexity: O(k+l) where: k - number of unique words of max length in the input sentence,
   * l - number of characters in the longest word.
   *
   * Explanation: O(k) to hold the result set + O(l) to hold the word in memory when it's built
   * during iteration over characters = O(k+l)
   *
   * @param sentence the sentence to search for longest words in; string with no words (whitespaces,
   * punctuation, etc.) is considered valid and returns a Result holding an empty set and 0 as
   * length.
   * @return a new LongestWords.Result holding the longest words from the sentence and the length.
   * @throws IllegalArgumentException if input sentence is null.
   */
  public Result findLongestWordsOptimized(String sentence) throws IllegalArgumentException {
    if (sentence == null) {
      throw new IllegalArgumentException("Input sentence cannot be null");
    }

    String trimmedSentence = sentence.trim();
    if (trimmedSentence.isEmpty()) {
      return new Result(new HashSet<String>());
    }

    // Find max length
    int maxLength = 0;
    int currentWordLength = 0;
    for (int i = 0; i < trimmedSentence.length(); i++) {
      if (Character.isLetter(sentence.charAt(i))) {
        currentWordLength++;
        if (currentWordLength > maxLength) {
          maxLength = currentWordLength;
        }
      } else {
        currentWordLength = 0;
      }
    }

    // Collect all words of maxLength
    Set<String> longestWords = new HashSet<String>();
    if (maxLength == trimmedSentence.length()) {
      // One-word input, just add it to longestWords, no need to iterate
      longestWords.add(trimmedSentence);
    } else {
      // Iterate through characters, build words, check against maxLength and add to result
      StringBuilder currentWord = new StringBuilder();
      for (int i = 0; i < trimmedSentence.length(); i++) {
        char currentCharacter = sentence.charAt(i);
        if (Character.isLetter(currentCharacter)) {
          currentWord.append(currentCharacter);
          if (currentWord.length() == maxLength) {
            longestWords.add(currentWord.toString());
          }
        } else {
          currentWord.setLength(0);
        }
      }
    }

    return new Result(longestWords);
  }
}
